/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

module.exports = [
    {
        code : "sex",
        name : "性别",
        options : [{
            code : "male",
            text : "男"
        },{
            code : "female",
            text : "女"
        }]
    },
];
