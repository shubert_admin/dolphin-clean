/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

module.exports = [
    {
        "__v" : 0,
        "_id" : "b8fa6284-6990-4798-84e6-2082fdbd4d3f",
        "code" : "admin",
        "menus" : [
            "33ee29c8-a240-43f0-abcd-4ec9c2142e65",
            "82e687c8-5a71-4174-898c-d317bc22e3b8",
            "36d7c2f2-699a-4ff6-93b9-5ca84230241a",
            "6926527e-7e1c-48c2-8995-ca0140b7deeb",
            "8910a997-e081-45a6-8dff-9ed1106c5456",
            "414c8785-567c-4abe-b4e0-5decfa1c5cab",
            "7c2f74e2-a9b9-499e-9604-6ea3c8b28a47",
        ],
        "name" : "管理员",
        "state" : 1,
    },
];
