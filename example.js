/**
 * Created by wangshuyi on 2017/3/2.
 */

'use strict';
(function () {
    function TreeList(param) {
        this.init(param);

        this.render();
    }

    TreeList.defaultParams = {
        //------------ 必传参数 ------------
        panel: '',                  //渲染位置
        columns: [],                //字段配置
        contentField: [],           //内容配置

        //------------ 可选参数 ------------
        resourceType: 'local',      //数据源，local：前台 / server：服务器
        data: {},                   //渲染数据
        url: '',                    //请求地址

        nameField: 'title',         //名称字段

        //------------ 监听事件 ------------
        onExpend: null              //展开内容时
    };

    TreeList.prototype = {
        constructor : TreeList,
    };

    TreeList.prototype.init = function (param) {
        this.opts = $.extend({}, TreeList.defaultParams, param);
    };
    TreeList.prototype.render = function () {
        //TODO 实现页面html渲染
    };
})();