$(function () {
   $('.panel.togglePanel').each(function () {
        var thisPanel = this;
        $(thisPanel).find('.panel-heading').click(function (e) {
            if ($(e.target).closest('.panel-operation').length == 0) {
                $(thisPanel).find('.panel-operation').toggle();
                $(thisPanel).find('.panel-body').slideToggle(500);
            }
        });
    });

    //提示
    $('[data-toggle="tooltip"]').tooltip();

    //通用按钮
    $('.dol-goBack').click(function () {
        history.go(-1);
    });
    $('.dol-hideModal').click(function () {
        $(this).closest('.modal').modal('hide');
    });
});

function parseCheckbox(name, checked){
    var div = $('<div class="slideThree">');
    var id = Dolphin.random(8);
    var input = $('<input type="checkbox">').attr({
        id : id,
        name : name
    }).appendTo(div);
    Dolphin.toggleCheck(input, checked);
    var label = $('<label>').attr('for', id).appendTo(div);
    return div;
}

function escape2Html(str) {
    var arrEntities={'lt':'<','gt':'>','nbsp':' ','amp':'&','quot':'"', 'amp;quot':'"'};
    return str.replace(/&(lt|gt|nbsp|amp;quot|quot|amp);/ig,function(all,t){return arrEntities[t];});
}

function bindSearchPanel(inputPanel, callback) {
    inputPanel.find('.searchInput').keypress(function (event) {
        if(event.keyCode == 13){
            callback();
        }
        event.stopPropagation();
        return false;
    });
    inputPanel.find('.searchButton').click(function () {
        callback();
        event.stopPropagation();
        return false;
    })
}