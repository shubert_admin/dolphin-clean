$(function () {
    //list
    var testList = new Dolphin.LIST({
        panel : "#testList",
        data : {total : 3, rows : [{
            firstName : 'Mark',
            lastName : 'Otto',
            username : 'mdo'
        },{
            firstName : 'Jacob',
            lastName : 'Thornton',
            username : 'fat'
        },{
            firstName : 'Larry',
            lastName : 'Bird',
            username : 'twitter'
        }]},
        columns : [{
            code : 'firstName',
            title : 'First Name',
            formatter : function(val, row, index){
                return val;
            }
        },{
            code : 'lastName',
            title : 'Last Name'
        },{
            code : 'username',
            title : 'Username'
        }]
    });

    //tree
    var testTree = new Dolphin.TREE({
        panel : "#testTree",
        data : [
            {
                id : 1,
                name : '节点1',
                __type : 'folder',
                children : [
                    {
                        id : 2,
                        name : '节点2',
                    },
                    {
                        id : 3,
                        name : '节点3',
                    },
                ]
            }
        ],
        onLoad: function (data) {
            this.expandAll();
        }
    });

    //form
    Dolphin.form.parse();
});