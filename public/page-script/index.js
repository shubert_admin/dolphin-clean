/**
 * Created by wangshuyi on 2017/2/4.
 */

'use strict';

const indexPage = {
    url: {
        Menu :  {
            tree : '/auth/menu/tree',
        },
    },

    init: null,
    initElement: null,
    initEvent: null,
};

indexPage.init = function () {
    this.initElement();
    this.initEvent();
};

indexPage.initElement = function () {
    const thisPage = this;

    let panel = $('#cl-list-body');
    let data = [{
        title: '标题1',
        content: '内容1',
        field1: '字段1',
        field2: '字段2',
        field3: '字段3',
    }, {
        title: '标题2',
        content: '内容2',
        field1: '字段1',
        field2: '字段2',
        field3: '字段3',
    }];

    let config = {
        nameField: 'title',
        contentField: [{
            field: 'content',
            type:'file',
        }],
        columns: [{
            code: 'field1',
        },{
            code: 'field2'
        },{
            code: 'field3'
        }],
    };

    data.forEach(function(d){
        let title = $('<tr>').addClass('cl-list-node').appendTo(panel);
        panel.append('内容');


        let name = $('<td>').appendTo(title);
        $('<span>').addClass('glyphicon').addClass('glyphicon-folder-close').click(function () {
            $(this).toggleClass('glyphicon-folder-close').toggleClass('glyphicon-folder-open').closest('tr').next().toggle();
        }).appendTo(name);
        name.append(d[config.nameField]);

        config.columns.forEach(function (col) {
            $('<td>').html(d[col.code]).appendTo(title);
        });

        let content = $('<tr>').addClass('cl-list-content default-hidden').appendTo(panel);
        let contentPanel = $('<td>').attr('colspan', config.columns.length + 1).appendTo(content);

        contentPanel.html(d[config.contentField]);
        config.contentField.forEach(function(){

        })
    });

};
indexPage.initEvent = function () {
    const thisPage = this;
};

$(function () {
    indexPage.init()
});