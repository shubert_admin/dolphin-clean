/**
 * Created by Shubert.Wang on 2016/1/22.
 */
'use strict';
$(function () {
    let orderNo;
    let getQrCodeState = function () {
        if(orderNo){
            Dolphin.ajax({
                url: '/qrcode/getState/'+orderNo,
                onSuccess: function (data) {
                    if(data.data == 'success'){
                        clearInterval(qrCodeTiming);
                        alert(data.massage);
                        history.go(-1);
                    }
                },
                onError: function () {
                    clearInterval(qrCodeStateTiming);
                }
            });
        }
    };
    let getQrCode = function () {
        getQrCodeState();
        Dolphin.ajax({
            url: '/qrcode',
            onSuccess: function (data) {
                $('img#qrCode').attr('src',data.data);

                JsBarcode("#barCode", data.code, {
                    text: data.code,
                    textPosition: 'top',
                });
                orderNo = data.orderNo;
                $('#orderNo').html(orderNo);
            },
            onError: function () {
                clearInterval(qrCodeTiming);
            }
        })
    };

    let qrCodeTiming = setInterval(getQrCode, 60000);
    let qrCodeStateTiming = setInterval(getQrCodeState, 5000);
    getQrCode();
});