/**
* Created by wangshuyi on 5/22/2017, 3:19:31 PM.
*/

'use strict';

const extend = require('extend');
const logger = require('log4js').getLogger("sys");

const CommonService = require("../../service/util/dbService");
const Model = require("../../module/auth/UserModel");

const defaultParams = {
    model : Model
};

class ModuleService extends CommonService{
    constructor(param){
        super(param);
        this.opts = extend(true, {}, this.opts, defaultParams, param);
        this._name = "UserService";
    }
}

module.exports = new ModuleService();


